#!/bin/zsh

local tt=$1

vev_monitors=$(xrandr -q | grep -w connected | awk '/connected/{print $1}')
if [[ $(echo $vev_monitors | wc -l) -eq 2 ]]; then

    vev_iei=$(mons | tail -n+3 | grep -w enabled)
    if [[ $(echo $vev_iei | wc -l) -eq 2 ]]; then
        echo "yay $tt"
        if [[ $tt == "w" ]]; then
            mons -o
        fi
        if [[ $tt == "q" ]]; then
            mons -s
        fi
    else
        echo "BOOB"
        mons -e left
    fi

    # TODO Build Conditions for merging on switch
    bspc monitor LVDS1 -n R
    bspc monitor VGA1 -n L
    bspc monitor R -s L
    bspc monitor L -d ""
    bspc monitor R -d ""
fi
